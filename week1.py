#coding=UTF-8

# set variable for initial value
#initialvalue = 1
#generate a infinite loop
while  True:
    initialValue = int(input("please input initial number:"))
    #set variable for final value
    #finalvalue = 50
    fnitialValue = int(input("please input final number:"))
    if(initialValue > 0 and finalValue> 0 \
    and initialValue < finalValue):
        break
    else:
        print("Inputs need to be greater than 0 and \
                \ninitial value less than final value.")
        
# initialize the sum variable
sum = 0

'''use for loop to iterate the number from initial value to
final value
'''
for number in range(initialValue, finalValue+1, 1):
    #check the number variable
    #print(number)
    #add each number into sum variable
    sum = sum + number
#print result
print(sum)