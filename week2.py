"""docstring
從 1 累加到 50, 總數是多少? 請將程式寫在 OpenShift 平台上, 採用 URL 輸入變數的模式編寫
"""
# 表單有中文要加上這個
_cp_config = {
    'tools.encode.encoding': 'utf-8'
}
def index(self):
    '''
    這裡是首頁喔!
    '''
    Outstring = '''
    <h1>從 start 累加到 end, 總數是多少</h1>
    <h2>假如任意一值為空或不為數字則會導回此頁面,大小填反系統會自動切換</h2>
    <form method="post" action="doAct">
    起始點:<input type=text name=start value=1 ><br />
    結束點:<input type=text name=end value=1 ><br />
    <input type="submit" value="send">
    <input type="reset" value="reset">
    </form>
    '''
    return Outstring
index.exposed = True
def doAct(self, start=None, end=None):

    total = 0
    if(start and end):
        try:
            start = int(start)
            end = int(end)
        except:
            raise cherrypy.HTTPRedirect("/")
            #return "some input is not correct<br />" + "<a href=\"/index\">Index</a>"
        if start>end:
            start, end = end, start
        for i in range(start, end + 1 ):
            total = total + i
        return "總和:" + str(total) + "<a href=\"/\">Index</a>"
    raise cherrypy.HTTPRedirect("/")
doAct.exposed = True